﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Text.Json;

namespace Projektuppgift_2IS052
{
    class Server
    {
        public byte[] IV { get; set; }
        public Dictionary<string, byte[]> Vault { get; set; }
        public string PathToServer { get; set; }
        public Server()
        {

        }

        public void Init(string server)
        {
            //Generera IV och spara i server.
            Aes myAes = Aes.Create();
            this.IV = myAes.IV;
            this.PathToServer = server;
            this.Vault = new Dictionary<string, byte[]>();

            string jsonStringServer = JsonSerializer.Serialize(this);
            var serverFile = File.Create(server);
            serverFile.Close();
            File.WriteAllText(server, jsonStringServer);
        }


    }
}
