﻿using System;
using System.Collections.Concurrent;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Projektuppgift_2IS052
{
    class Program
    {
        static void Main(string[] args)
        {
            StartUp();
            Init("client.json","server.json", "123");
        }
        static void StartUp()
        {
            Console.WriteLine("----Welcome to passwordman----");
            Console.WriteLine("----See docs for instructions----");
        }
        //Create new vault
        static void Init(string client, string server, string pwd)
        {
            Client newClient = new Client();
            newClient.Init(client);

            Server newServer = new Server();
            newServer.Init(server);
            
           
            //Kombinera secretkey + pwd för att få key
            byte[] pwdByte = Encoding.ASCII.GetBytes(pwd);
            Rfc2898DeriveBytes rfc = new Rfc2898DeriveBytes(pwdByte, newClient.SecretKey, 1000);
            //OSäker på algos
            Console.WriteLine(rfc.GetBytes(16));
            byte[] temp = rfc.GetBytes(16);
            Console.WriteLine("");



        }

    }
}
