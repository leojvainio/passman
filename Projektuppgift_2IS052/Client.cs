﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text.Json;
using System.Text.Json.Serialization;


namespace Projektuppgift_2IS052
{
    class Client
    {
        public byte[] SecretKey { get; set; }
        public string PathToClient { get; set; }

        public Client()
        {
            

        }

        public void Init(string client)
        {
            RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();
            byte[] secretkey = new byte[16];
            rng.GetBytes(secretkey);
            var clientFile = File.Create(client);
            clientFile.Close();

            this.SecretKey = secretkey;
            this.PathToClient = client;
                
            string jsonStringSecretKey = JsonSerializer.Serialize(this);
            File.WriteAllText(client, jsonStringSecretKey);
        }

        public void LoadClient(string client)
        {
            string jsonString = File.ReadAllText(client);
            Client client1 = JsonSerializer.Deserialize<Client>(jsonString);
            this.PathToClient = client1.PathToClient;
            this.SecretKey = client1.SecretKey;
        }
    }
}
